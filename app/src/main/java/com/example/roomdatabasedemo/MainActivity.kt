package com.example.roomdatabasedemo

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.roomdatabasedemo.databinding.ActivityMainBinding

const val TAG = "logcat"

class MainActivity : AppCompatActivity(), OnEditClicked{

    private lateinit var binding: ActivityMainBinding

    private val userDB: UserDatabase by lazy {
        Room.databaseBuilder(this, UserDatabase::class.java, "user_database")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    private lateinit var userAdapter:UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAddUser.setOnClickListener {
            startActivity(Intent(this, AddUserActivity::class.java))
        }
        userAdapter = UserAdapter()
        userAdapter.setEditListener(this)
        checkItem()
    }

    private fun checkItem() {
        binding.apply {
            if (userDB.userDao().getAllUser().isNotEmpty()) {
                userAdapter.differ.submitList(userDB.userDao().getAllUser())
                userAdapter.notifyDataSetChanged()
                setupRecyclerView()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause: ")
    }

    override fun onResume() {
        super.onResume()
        checkItem()
        Log.d(TAG, "onResume: ")
    }

    private fun setupRecyclerView(){
        binding.userRecyclerView.apply {
            layoutManager=LinearLayoutManager(this@MainActivity)
            adapter=userAdapter

        }

    }

    override fun getEditClicked(position: Int, user: User) {
        val intent =Intent(this,EditUserActivity::class.java)
        intent.putExtra("userId",user.id)
        startActivity(intent)
    }
}