package com.example.roomdatabasedemo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdatabasedemo.databinding.ListUserBinding

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>(){
    private lateinit var binding: ListUserBinding
    private lateinit var context: Context
    private var listener: OnEditClicked? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ListUserBinding.inflate(inflater, parent, false)
        context = parent.context
        return ViewHolder()
    }

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        holder.bind(differ.currentList[position],position)
        holder.setIsRecyclable(false)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(user: User,position: Int) {
            //InitView
            binding.apply {
                //Set text
                tvName.text = user.name
                tvAge.text= user.age.toString()
                tvAddress.text = user.address
                btnEdit.setOnClickListener {
                    listener?.getEditClicked(position,user)
                }
            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    fun setEditListener(listener: OnEditClicked){
        this.listener = listener
    }

}

interface OnEditClicked {
    fun getEditClicked(position: Int, user: User)
}
