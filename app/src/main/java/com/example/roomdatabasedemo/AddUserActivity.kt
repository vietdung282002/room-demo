package com.example.roomdatabasedemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.roomdatabasedemo.databinding.ActivityAddUserBinding

class AddUserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddUserBinding
    private val userDB: UserDatabase by lazy {
        Room.databaseBuilder(
            this,
            UserDatabase::class.java,
            "user_database"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
    }
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            btnSave.setOnClickListener {
                val name = itemName.text
                val age = itemAge.text
                val address = itemAddress.text
                if(name == null){
                    itemName.error = "Name can't be Empty"
                }
                if(age == null){
                    itemAge.error = "Age can't be Empty"
                }
                if(address == null){
                    itemAddress.error = "itemAddress can't be Empty"
                }
                if(name != null && age != null && address != null){
                    user = User(0,name.toString(),Integer.parseInt(age.toString()),address.toString())
                    userDB.userDao().insertUser(user)
                    finish()
                }
            }
        }
        binding.btnCancel.setOnClickListener {
            finish()
        }
    }
}