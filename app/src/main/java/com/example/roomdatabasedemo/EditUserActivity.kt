package com.example.roomdatabasedemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.room.Room
import com.example.roomdatabasedemo.databinding.ActivityEditUserBinding

class EditUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityEditUserBinding
    private val userDB: UserDatabase by lazy {
        Room.databaseBuilder(
            this,
            UserDatabase::class.java,
            "user_database"
        ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
    }
    private lateinit var user: User
    private var userId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.extras?.let {
            userId = it.getInt("userId")
        }
        binding.apply {
            itemName.setText(userDB.userDao().getUser(userId).name)
            itemAge.setText(userDB.userDao().getUser(userId).age.toString())
            itemAddress.setText(userDB.userDao().getUser(userId).address)
            btnSave.setOnClickListener {
                if(itemName.text == null){
                    itemName.error = "Name can't be Empty"
                }
                if(itemAge.text == null){
                    itemAge.error = "Age can't be Empty"
                }
                if(itemAge.text == null){
                    itemAddress.error = "itemAddress can't be Empty"
                }
                if(itemName.text != null && itemAge.text != null && itemAddress.text != null){
                    user = User(userId,itemName.text.toString(),Integer.parseInt(itemAge.text.toString()),itemAddress.text.toString())
                    userDB.userDao().update(user)
                    finish()
                }
            }
            btnCancel.setOnClickListener {
                finish()
            }
            btnDelete.setOnClickListener {
                user = User(userId,userDB.userDao().getUser(userId).name,userDB.userDao().getUser(userId).age,userDB.userDao().getUser(userId).address)
                userDB.userDao().delete(user)
                finish()
            }
        }
    }
}